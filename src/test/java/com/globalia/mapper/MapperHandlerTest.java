package com.globalia.mapper;

import com.globalia.json.JsonHandler;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MapperHandlerTest {

	@InjectMocks
	private MapperHandler mapper;
	@Mock
	private MonitorHandler handler;
	@Mock
	private JsonHandler jsonHandler;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testAddError() {
		assertNotNull(this.mapper.addError("test", new Monitor(), "class", "method", 1));
	}
}
