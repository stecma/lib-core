package com.globalia.security;

import com.globalia.api.ApiRS;
import com.globalia.enumeration.LogType;
import com.globalia.json.JsonHandler;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class JwtTokenFilterTest {

	private static final String SECRET_KEY = "JwtSecret0123456";
	private static final String ALGORITHM = "AES";
	private static final String SERVICE_TEST = "service";
	private static final String PORT_KEY = "port";

	@InjectMocks
	private JwtTokenFilter jwtTokenFilter;
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private FilterChain filterChain;
	@Mock
	private JwtTokenProvider jwtTokenProvider;
	@Mock
	private MonitorHandler handler;
	@Mock
	private JsonHandler jsonHandler;

	private Monitor monitor;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);

		this.jwtTokenFilter.init(LogType.DEBUG, JwtTokenFilterTest.SECRET_KEY, JwtTokenFilterTest.SECRET_KEY, JwtTokenFilterTest.ALGORITHM, JwtTokenFilterTest.SERVICE_TEST, JwtTokenFilterTest.PORT_KEY);
		this.monitor = new Monitor();
		this.monitor.setLogs(new HashSet<>());
		this.monitor.setStats(new HashSet<>());
		when(this.jwtTokenProvider.getHandler()).thenReturn(this.handler);
	}

	@Test
	public void testDoFilterInternalValidateException() throws IOException {
		when(this.handler.create(nullable(String.class), anyString(), anyString())).thenReturn(this.monitor);
		when(this.request.getHeader(anyString())).thenThrow(NullPointerException.class);
		when(this.jwtTokenProvider.getHandler().create(anyString(), anyString(), anyString())).thenReturn(this.monitor);
		when(this.jsonHandler.toJson(any(ApiRS.class))).thenReturn("api");

		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(this.response.getWriter()).thenReturn(writer);

		this.jwtTokenFilter.doFilterInternal(this.request, this.response, this.filterChain);
		assertTrue(true);
	}

	@Test
	public void testDoFilterInternal() throws ServletException, IOException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
		when(this.request.getHeader(anyString())).thenReturn("Bearer Token");
		when(this.request.getRequestURI()).thenReturn("test");

		when(this.jwtTokenProvider.getHandler().create(anyString(), anyString(), anyString())).thenReturn(this.monitor);
		when(this.jsonHandler.toJson(any(ApiRS.class))).thenReturn("api");
		when(this.jwtTokenProvider.getAuthentication(anyString(), any(), any())).thenReturn(new UsernamePasswordAuthenticationToken("user", "pass"));

		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		when(this.response.getWriter()).thenReturn(writer);
		this.jwtTokenFilter.doFilterInternal(this.request, this.response, this.filterChain);
		assertTrue(true);
	}

	@Test
	public void testDoFilterInternalEmptyToken() throws IOException {
		String bearer = "";
		when(this.request.getHeader("Authorization")).thenReturn(bearer);

		this.jwtTokenFilter.doFilterInternal(this.request, this.response, this.filterChain);
		assertTrue(true);
	}
}
