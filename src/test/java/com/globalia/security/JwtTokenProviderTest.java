package com.globalia.security;

import com.globalia.Context;
import com.globalia.configuration.TokenConfig;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.dto.login.Url;
import com.globalia.dto.login.UserItem;
import com.globalia.dto.login.UserMenu;
import com.globalia.enumeration.LogType;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@SuppressWarnings({"ResultOfMethodCallIgnored", "unused"})
@RunWith(MockitoJUnitRunner.Silent.class)
public class JwtTokenProviderTest {

	private static final String SECRET_KEY = "JwtSecret0123456";
	private static final int EXPIRATION_MS = 360000;
	private static final int EXPIRATION_ADMIN_MS = 360000;
	private static final String JSON_SERVICE = "{\"url\":\"service1\"}";
	private static final String SERVICE_ONE = "service1";
	private static final String EXAMPLE = "exampleService";
	private static final String USER = "User";
	private static final String PASS = "Pass";

	@InjectMocks
	private JwtTokenProvider jwtTokenProvider;
	@Mock
	private HttpServletRequest request;
	@Mock
	private AESPassEncrypter encrypter;
	@Mock
	private MonitorHandler handler;
	@Mock
	private Context context;

	private String token;
	private String tokenAdmin;
	private Monitor monitor;
	private TokenConfig tokenConfig;

	@Before
	public void beforeClassFunction() {
		MockitoAnnotations.openMocks(this);

		this.monitor = new Monitor();
		this.monitor.setLogs(new HashSet<>());
		this.monitor.setStats(new HashSet<>());

		this.tokenConfig = new TokenConfig();
		this.tokenConfig.setSecret(JwtTokenProviderTest.SECRET_KEY);
		this.tokenConfig.setSecretEncrypt(JwtTokenProviderTest.SECRET_KEY);
		this.tokenConfig.setExpirationAdminInMs(JwtTokenProviderTest.EXPIRATION_ADMIN_MS);
		this.tokenConfig.setExpirationInMs(JwtTokenProviderTest.EXPIRATION_MS);
		this.tokenConfig.setAllowedLogLevel(LogType.DEBUG);
		this.tokenConfig.setForcedUrl("{\"routes\": [ { \"uri\": \"/api/v1/refresh\", \"readOnly\": true }, { \"uri\": \"/api/v1/validate\", \"readOnly\": true } ] }");

		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		this.token = this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor);
		user.setAdmin(true);
		this.tokenAdmin = this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor);

		this.jwtTokenProvider.getEncrypter();
		this.jwtTokenProvider.getHandler();
	}

	@Test(expected = ValidateException.class)
	public void testCreateTokenUserNull() {
		this.jwtTokenProvider.createToken(null, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testCreateTokenUserIdNull() {
		UserItem user = new UserItem();
		this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testCreateTokenUserPassNull() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testCreateTokenConfigSecretNull() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);

		this.tokenConfig.setSecret(null);
		this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testCreateTokenConfigExpirationAdminZero() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);

		this.tokenConfig.setExpirationAdminInMs(0);
		this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testCreateTokenConfigExpirationZero() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);

		this.tokenConfig.setExpirationInMs(0);
		this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testCreateTokenConfigNull() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		this.jwtTokenProvider.createToken(user, null, this.monitor);
	}

	@Test
	public void testCreateToken() {
		ServiceItem service = new ServiceItem();
		service.setUrl(new Url());
		service.getUrl().setRoute(JwtTokenProviderTest.EXAMPLE);
		service.getUrl().setReadOnly(true);

		GroupItem group = new GroupItem();
		group.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu2 = new MenuItem();
		menu2.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu2.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu = new MenuItem();
		menu.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu.setServices(new LinkedHashSet<>(Set.of(service)));

		UserMenu userMenu = new UserMenu();
		userMenu.setMenus(new LinkedHashSet<>());
		userMenu.setEnvironment("DEV");
		userMenu.getMenus().add(menu);
		userMenu.getMenus().add(menu2);

		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(false);
		user.setConfiguration(new LinkedHashSet<>());
		user.getConfiguration().add(userMenu);

		when(this.context.getEnvironment()).thenReturn("dev");
		assertNotNull(this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor));
	}

	@Test
	public void testCreateAdminToken() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(true);

		assertNotNull(this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor));
	}

	@Test(expected = ValidateException.class)
	public void testGetUserTokenNull() {
		this.jwtTokenProvider.getUser(null, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testGetUserConfigNull() {
		this.jwtTokenProvider.getUser(this.token, null, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testGetUserConfigSecretNull() {
		this.tokenConfig.setSecret(null);
		this.jwtTokenProvider.getUser(this.token, this.tokenConfig, this.monitor);
	}

	@Test
	public void testGetUser() {
		assertEquals(JwtTokenProviderTest.USER, this.jwtTokenProvider.getUser(this.token, this.tokenConfig, this.monitor));
	}

	@Test(expected = ValidateException.class)
	public void testRefreshTokenConfigNull() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);

		this.jwtTokenProvider.refreshToken(this.token, user, null, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testRefreshSecretNull() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		this.tokenConfig.setSecret(null);
		this.jwtTokenProvider.refreshToken(null, user, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testRefreshExpirationAdminZeroNull() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		this.tokenConfig.setExpirationAdminInMs(0);
		this.jwtTokenProvider.refreshToken(null, user, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testRefreshExpirationZeroNull() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		this.tokenConfig.setExpirationInMs(0);
		this.jwtTokenProvider.refreshToken(null, user, this.tokenConfig, this.monitor);
	}

	@Test
	public void testRefreshToken() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);

		assertNotNull(this.jwtTokenProvider.refreshToken(this.token, user, this.tokenConfig, this.monitor));
	}

	@Test
	public void testRefreshAdminToken() {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);

		assertNotNull(this.jwtTokenProvider.refreshToken(this.tokenAdmin, user, this.tokenConfig, this.monitor));
	}

	@Test(expected = ValidateException.class)
	public void testRefreshExpirationTokenConfigNull() {
		this.jwtTokenProvider.refreshExpiration(this.token, null, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testRefreshExpirationSecretNull() {
		this.tokenConfig.setSecret(null);
		this.jwtTokenProvider.refreshExpiration(null, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testRefreshExpirationAdminZero() {
		this.tokenConfig.setExpirationAdminInMs(0);
		this.jwtTokenProvider.refreshExpiration(null, this.tokenConfig, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testRefreshExpirationZero() {
		this.tokenConfig.setExpirationInMs(0);
		this.jwtTokenProvider.refreshExpiration(null, this.tokenConfig, this.monitor);
	}

	@Test
	public void testRefreshExpirationAdmin() throws InterruptedException {
		synchronized (this.jwtTokenProvider) {
			this.jwtTokenProvider.wait(1000);
		}
		assertNotEquals(this.tokenAdmin, this.jwtTokenProvider.refreshExpiration(this.tokenAdmin, this.tokenConfig, this.monitor));
	}

	@Test
	public void testRefreshExpiration() throws InterruptedException {
		synchronized (this.jwtTokenProvider) {
			this.jwtTokenProvider.wait(1000);
		}
		assertNotEquals(this.token, this.jwtTokenProvider.refreshExpiration(this.token, this.tokenConfig, this.monitor));
	}

	@Test(expected = ValidateException.class)
	public void testValidTokenJWTException() throws ServletException {
		this.jwtTokenProvider.tokenValidation(this.token, null, null, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testValidTokenSecretNullJWTException() throws ServletException {
		this.tokenConfig.setSecret(null);
		this.jwtTokenProvider.tokenValidation(this.token, null, this.tokenConfig, this.monitor);
	}

	@Test(expected = ServletException.class)
	public void testValidTokenEmptyException() throws ServletException {
		this.jwtTokenProvider.tokenValidation(null, null, this.tokenConfig, this.monitor);
	}

	@Test(expected = ServletException.class)
	public void testValidTokenExpiredException() throws ServletException {
		String newToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJVc2VyIiwic2hhIjoiUGFzcyIsImFkbSI6ZmFsc2UsImlhdCI6MTU3MDg3ODUwOSwiZXhwIjoxNTcwODU4ODY5fQ.R5dWFXwsWq4dPvam8796Sfgi1SjjLD2PtqPeooS7460";
		this.jwtTokenProvider.tokenValidation(newToken, null, this.tokenConfig, this.monitor);
	}

	@Test(expected = ServletException.class)
	public void testValidTokenSecretChangeException() throws ServletException {
		String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJVc2VyIiwic2hhIjoiMVNqVFVycmdvWFhGWGEwTUNMb2JIZz09IiwiYWRtIjpmYWxzZSwiaWF0IjoxNTY1NzgxMDQ2LCJleHAiOjE1NjYzODU4NDZ9.AzO6OJr3TgFC7MGodX0ef-ZfdwiA1SdfKNPjrsOFvZQ";
		this.jwtTokenProvider.tokenValidation(token, null, this.tokenConfig, this.monitor);
	}

	@Test(expected = ServletException.class)
	public void testValidTokenMalformException() throws ServletException {
		String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJVc2VyIiwic2hhIjoiMVNqVFVycmdvWFhGWGEwTUNMb2JIZz09IiwiYWRtIjpmYWxzZSwiaWF0IjoxNTY1NzgxMDQ2LCJleHAiOjE1NjYzODU4NDZ9";
		this.jwtTokenProvider.tokenValidation(token, null, this.tokenConfig, this.monitor);
	}

	@Test(expected = ServletException.class)
	public void testValidateNullEnvironmentToken() throws ServletException {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(false);

		String token = this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor);
		int size = this.monitor.getLogs().size();
		this.jwtTokenProvider.tokenValidation(token, this.request, this.tokenConfig, this.monitor);
		assertEquals(size, this.monitor.getLogs().size());
	}

	@Test(expected = ServletException.class)
	public void testValidateEnvironmentToken() throws ServletException {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(false);

		when(this.context.getEnvironment()).thenReturn("local");
		String token = this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor);
		when(this.context.getEnvironment()).thenReturn("dev");
		int size = this.monitor.getLogs().size();
		this.jwtTokenProvider.tokenValidation(token, this.request, this.tokenConfig, this.monitor);
		assertEquals(size, this.monitor.getLogs().size());
	}

	@Test(expected = ServletException.class)
	public void testValidateTokenNoServiceException() throws ServletException {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(false);

		this.tokenConfig.setForcedUrl(null);
		when(this.context.getEnvironment()).thenReturn("local");
		this.jwtTokenProvider.tokenValidation(this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor), this.request, this.tokenConfig, this.monitor);
	}

	@Test(expected = ServletException.class)
	public void testValidateTokenInvalidUriException() throws ServletException {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(false);

		when(this.request.getRequestURI()).thenReturn("service2");
		when(this.request.getMethod()).thenReturn("GET");
		when(this.context.getEnvironment()).thenReturn("dev");
		this.jwtTokenProvider.tokenValidation(this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor), this.request, this.tokenConfig, this.monitor);
	}

	@Test
	public void testValidateTokenUriReadAccess() throws ServletException {
		ServiceItem service = new ServiceItem();
		service.setUrl(new Url());
		service.getUrl().setRoute(JwtTokenProviderTest.SERVICE_ONE);
		service.getUrl().setReadOnly(true);

		GroupItem group = new GroupItem();
		group.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu2 = new MenuItem();
		menu2.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu2.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu = new MenuItem();
		menu.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu.setServices(new LinkedHashSet<>(Set.of(service)));

		UserMenu userMenu = new UserMenu();
		userMenu.setEnvironment("DEV");
		userMenu.setMenus(new LinkedHashSet<>());
		userMenu.getMenus().add(menu);
		userMenu.getMenus().add(menu2);

		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(false);
		user.setConfiguration(new LinkedHashSet<>());
		user.getConfiguration().add(userMenu);

		when(this.request.getRequestURI()).thenReturn(JwtTokenProviderTest.SERVICE_ONE);
		when(this.request.getMethod()).thenReturn("GET");
		when(this.context.getEnvironment()).thenReturn("dev");
		int size = this.monitor.getLogs().size();
		this.jwtTokenProvider.tokenValidation(this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor), this.request, this.tokenConfig, this.monitor);
		assertEquals(size, this.monitor.getLogs().size());
	}

	@Test
	public void testValidateTokenUriAdminAccess() throws ServletException {
		ServiceItem service = new ServiceItem();
		service.setUrl(new Url());
		service.getUrl().setRoute(JwtTokenProviderTest.SERVICE_ONE);
		service.getUrl().setReadOnly(false);

		GroupItem group = new GroupItem();
		group.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu2 = new MenuItem();
		menu2.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu2.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu = new MenuItem();
		menu.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu.setServices(new LinkedHashSet<>(Set.of(service)));

		UserMenu userMenu = new UserMenu();
		userMenu.setEnvironment("DEV");
		userMenu.setMenus(new LinkedHashSet<>());
		userMenu.getMenus().add(menu);
		userMenu.getMenus().add(menu2);

		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(false);
		user.setConfiguration(new LinkedHashSet<>());
		user.getConfiguration().add(userMenu);

		when(this.request.getRequestURI()).thenReturn(JwtTokenProviderTest.SERVICE_ONE);
		when(this.request.getMethod()).thenReturn("GET");
		when(this.context.getEnvironment()).thenReturn("dev");
		int size = this.monitor.getLogs().size();
		this.jwtTokenProvider.tokenValidation(this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor), this.request, this.tokenConfig, this.monitor);
		assertEquals(size, this.monitor.getLogs().size());
	}

	@Test(expected = ServletException.class)
	public void testValidateTokenPostReadAccess() throws ServletException {
		ServiceItem service = new ServiceItem();
		service.setUrl(new Url());
		service.getUrl().setRoute(JwtTokenProviderTest.EXAMPLE);
		service.getUrl().setReadOnly(true);

		GroupItem group = new GroupItem();
		group.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu2 = new MenuItem();
		menu2.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu2.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu = new MenuItem();
		menu.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu.setServices(new LinkedHashSet<>(Set.of(service)));

		UserMenu userMenu = new UserMenu();
		userMenu.setEnvironment("DEV");
		userMenu.setMenus(new LinkedHashSet<>());
		userMenu.getMenus().add(menu);
		userMenu.getMenus().add(menu2);

		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(false);
		user.setConfiguration(new LinkedHashSet<>());
		user.getConfiguration().add(userMenu);

		when(this.request.getRequestURI()).thenReturn("service1/add");
		when(this.request.getMethod()).thenReturn("POST");
		when(this.context.getEnvironment()).thenReturn("dev");
		int size = this.monitor.getLogs().size();
		this.jwtTokenProvider.tokenValidation(this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor), this.request, this.tokenConfig, this.monitor);
		assertEquals(size, this.monitor.getLogs().size());
	}

	@Test
	public void testValidateToken() throws ServletException {
		ServiceItem service = new ServiceItem();
		service.setUrl(new Url());
		service.getUrl().setRoute(JwtTokenProviderTest.EXAMPLE);
		service.getUrl().setReadOnly(true);

		GroupItem group = new GroupItem();
		group.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu2 = new MenuItem();
		menu2.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu2.setServices(new LinkedHashSet<>(Set.of(service)));

		MenuItem menu = new MenuItem();
		menu.setGroups(new LinkedHashSet<>(Set.of(group)));
		menu.setServices(new LinkedHashSet<>(Set.of(service)));

		UserMenu userMenu = new UserMenu();
		userMenu.setEnvironment("DEV");
		userMenu.setMenus(new LinkedHashSet<>());
		userMenu.getMenus().add(menu);
		userMenu.getMenus().add(menu2);

		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(false);
		user.setConfiguration(new LinkedHashSet<>());
		user.getConfiguration().add(userMenu);

		when(this.request.getRequestURI()).thenReturn(JwtTokenProviderTest.EXAMPLE);
		when(this.request.getMethod()).thenReturn("POST");
		when(this.context.getEnvironment()).thenReturn("dev");
		int size = this.monitor.getLogs().size();
		this.jwtTokenProvider.tokenValidation(this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor), this.request, this.tokenConfig, this.monitor);
		assertEquals(size, this.monitor.getLogs().size());
	}

	@Test
	public void testValidateTokenAdmin() throws ServletException {
		UserItem user = new UserItem();
		user.setId(JwtTokenProviderTest.USER);
		user.setPass(JwtTokenProviderTest.PASS);
		user.setAdmin(true);

		when(this.request.getRequestURI()).thenReturn(JwtTokenProviderTest.EXAMPLE);
		when(this.request.getMethod()).thenReturn("POST");
		when(this.context.getEnvironment()).thenReturn("dev");
		int size = this.monitor.getLogs().size();
		this.jwtTokenProvider.tokenValidation(this.jwtTokenProvider.createToken(user, this.tokenConfig, this.monitor), this.request, this.tokenConfig, this.monitor);
		assertEquals(size, this.monitor.getLogs().size());
	}

	@Test(expected = ValidateException.class)
	public void testAuthenticationJWTNullException() throws ServletException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
		this.jwtTokenProvider.getAuthentication(null, null, this.monitor);
	}

	@Test(expected = ServletException.class)
	public void testAuthenticationException() throws ServletException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
		this.jwtTokenProvider.getAuthentication(null, this.tokenConfig, this.monitor);
	}

	@Test
	public void testAuthentication() throws ServletException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
		int size = this.monitor.getLogs().size();
		this.jwtTokenProvider.getAuthentication(this.token, this.tokenConfig, this.monitor);
		assertEquals(size, this.monitor.getLogs().size());
	}
}
