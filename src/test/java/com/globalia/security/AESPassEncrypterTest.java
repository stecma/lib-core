package com.globalia.security;

import com.globalia.enumeration.LogType;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AESPassEncrypterTest {

	private static final String SECRET_KEY = "JwtSecret0123456";
	private static final String ALGORITHM = "AES";

	@InjectMocks
	private AESPassEncrypter encrypter;
	@Mock
	private MonitorHandler handler;

	private Monitor monitor;

	@Before
	public void beforeClassFunction() {
		MockitoAnnotations.openMocks(this);

		this.monitor = new Monitor();
		this.monitor.setLogs(new HashSet<>());
		this.monitor.setStats(new HashSet<>());
	}

	@Test(expected = Exception.class)
	public void testEncryptException() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		this.encrypter.encrypt(null, AESPassEncrypterTest.SECRET_KEY, AESPassEncrypterTest.ALGORITHM, this.monitor, LogType.DEBUG);
	}

	@Test
	public void testEncrypt() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		String result = this.encrypter.encrypt("passSinEncriptar", AESPassEncrypterTest.SECRET_KEY, AESPassEncrypterTest.ALGORITHM, this.monitor, LogType.DEBUG);
		assertEquals("NMbF/UkFrkynjRxN4+A0buU3a5JvaB5Bha94ZuMq23M=", result);
	}

	@Test(expected = Exception.class)
	public void testDecrypException() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		this.encrypter.decrypt(null, AESPassEncrypterTest.SECRET_KEY, AESPassEncrypterTest.ALGORITHM, this.monitor, LogType.DEBUG);
	}

	@Test
	public void testDecrypt() throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		String result = this.encrypter.decrypt("NMbF/UkFrkynjRxN4+A0buU3a5JvaB5Bha94ZuMq23M=", AESPassEncrypterTest.SECRET_KEY, AESPassEncrypterTest.ALGORITHM, this.monitor, LogType.DEBUG);
		assertEquals("passSinEncriptar", result);
	}
}
