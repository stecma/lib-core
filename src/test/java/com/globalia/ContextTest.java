package com.globalia;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ContextTest {

	@InjectMocks
	private Context context;
	@Mock
	private Environment environment;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetEnvironmentDft() {
		String[] profile = new String[1];
		profile[0] = "";
		when(this.environment.getActiveProfiles()).thenReturn(profile);
		assertEquals("dft", this.context.getEnvironment());
	}

	@Test
	public void testGetEnvironment() {
		String[] profile = new String[2];
		profile[0] = "kubernetes";
		profile[1] = "dev";
		when(this.environment.getActiveProfiles()).thenReturn(profile);
		assertEquals("dev", this.context.getEnvironment());
	}

	@Test
	public void testGetEnvironments() {
		String[] profile = new String[1];
		profile[0] = "kubernetes";
		when(this.environment.getActiveProfiles()).thenReturn(profile);
		assertFalse(this.context.getEnvironments().contains(","));
	}

	@Test
	public void testGetEnvironmentsContains() {
		String[] profile = new String[2];
		profile[0] = "kubernetes";
		profile[1] = "dev";
		when(this.environment.getActiveProfiles()).thenReturn(profile);
		assertTrue(this.context.getEnvironments().contains(","));
	}
}
