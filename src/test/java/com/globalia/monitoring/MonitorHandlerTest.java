package com.globalia.monitoring;

import com.globalia.enumeration.LogType;
import com.globalia.enumeration.StatType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MonitorHandlerTest {

	private static final String HOST = "host";
	private static final String TEST = "test";
	private static final String SERVICE_TEST = "servicetest";

	@InjectMocks
	private MonitorHandler handler;

	private Monitor monitor;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);

		this.monitor = new Monitor();
		this.monitor.setCorrelationID("1");
		this.monitor.setTimestamp(System.currentTimeMillis());
		this.monitor.setRoute("routetest");
		this.monitor.setLogs(new HashSet<>());

		Log log = new Log();
		log.setTimestamp(System.currentTimeMillis());
		log.setType(LogType.INFO);
		log.setHost(MonitorHandlerTest.HOST);
		log.setMethod(MonitorHandlerTest.TEST);
		log.setMessage(MonitorHandlerTest.TEST);
		log.setService(MonitorHandlerTest.SERVICE_TEST);

		Log log2 = new Log();
		log2.setTimestamp(System.currentTimeMillis());
		log2.setType(LogType.DEBUG);
		log2.setHost(MonitorHandlerTest.HOST);
		log2.setMethod(MonitorHandlerTest.TEST);
		log2.setMessage(MonitorHandlerTest.TEST);
		log2.setService(MonitorHandlerTest.SERVICE_TEST);

		this.monitor.getLogs().add(log);
		this.monitor.getLogs().add(log2);
		this.monitor.setStats(new HashSet<>());
		Stat stat = new Stat();
		stat.setType(StatType.UNMARSHALL);
		stat.setHost(MonitorHandlerTest.HOST);
		stat.setElapsedMS(System.currentTimeMillis());
		stat.setService(MonitorHandlerTest.SERVICE_TEST);

		Stat stat2 = new Stat();
		stat2.setType(StatType.SQL_QUERY);
		stat2.setHost(MonitorHandlerTest.HOST);
		stat2.setElapsedMS(System.currentTimeMillis());
		stat2.setService(MonitorHandlerTest.SERVICE_TEST);

		this.monitor.getStats().add(stat);
		this.monitor.getStats().add(stat2);
	}

	@Test
	public void testCreateRequestProxyMonitor() {
		assertNotNull(this.handler.create("request", "service", "8080"));
	}

	@Test
	public void testCreateRequestMonitor() {
		assertNotNull(this.handler.create(null, "service", "8080"));
	}

	@Test
	public void testCreateRequestOfuscateRQ() {
		assertNotNull(this.handler.create("{\"pass\":\"***********\",\"user\": \"34064792K\"}", "service", "8080"));
	}

	@Test
	public void testAddDebugLog() {
		int size = this.monitor.getLogs().size();
		this.handler.addDebugLog(this.monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), MonitorHandlerTest.TEST, LogType.DEBUG);
		assertEquals(size + 1, this.monitor.getLogs().size());
	}

	@Test
	public void testAddInfoLog() {
		int size = this.monitor.getLogs().size();
		this.handler.addInfoLog(this.monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), MonitorHandlerTest.TEST, LogType.INFO);
		assertEquals(size + 1, this.monitor.getLogs().size());
	}

	@Test
	public void testAddWarnLog() {
		int size = this.monitor.getLogs().size();
		this.handler.addWarnLog(this.monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), MonitorHandlerTest.TEST, LogType.WARN);
		assertEquals(size + 1, this.monitor.getLogs().size());
	}

	@Test
	public void testAddErrorLog() {
		int size = this.monitor.getLogs().size();
		this.handler.addErrorLog(this.monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), MonitorHandlerTest.TEST, LogType.ERROR);
		assertEquals(size + 1, this.monitor.getLogs().size());
	}

	@Test
	public void testAddStatHostException() {
		int size = this.monitor.getStats().size();
		this.handler.addStat(this.monitor, StatType.MARSHALL, System.currentTimeMillis());
		assertEquals(size + 1, this.monitor.getStats().size());
	}

	@Test
	public void testAddStat() {
		int size = this.monitor.getStats().size();
		this.handler.addStat(this.monitor, StatType.MARSHALL, System.currentTimeMillis());
		assertEquals(size + 1, this.monitor.getStats().size());
	}

	@Test
	public void testToJsonNull() {
		assertNull(this.handler.toJson(null));
	}

	@Test
	public void testToJsonNullContent() {
		this.monitor.setStats(null);
		this.monitor.setLogs(null);
		assertNotNull(this.handler.toJson(this.monitor));
	}

	@Test
	public void testToJson() {
		assertNotNull(this.handler.toJson(this.monitor));
	}

	@Test
	public void testSortedLogsNull() {
		assertNull(this.handler.sortedLogs(null));
	}

	@Test
	public void testSortedLogs() {
		assertNotNull(this.handler.sortedLogs(this.monitor.getLogs()));
	}

	@Test
	public void testSorteStatsNull() {
		assertNull(this.handler.sortedStats(null));
	}

	@Test
	public void testSorteStats() {
		assertNotNull(this.handler.sortedStats(this.monitor.getStats()));
	}
}
