package com.globalia;

import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.HashSet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DatesTest {

	@InjectMocks
	private Dates dates;

	private Monitor monitor;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);

		this.monitor = new Monitor();
		this.monitor.setLogs(new HashSet<>());
		this.monitor.setStats(new HashSet<>());
	}

	@Test(expected = ValidateException.class)
	public void testValidateRangeParseError() {
		this.dates.validateDateRange("20/15/2020", null, this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testValidateRangeInvalid() {
		this.dates.validateDateTimeRange("20/06/2020", "19/06/2020", "00:00:00", "23:59:59", this.monitor);
	}

	@Test(expected = ValidateException.class)
	public void testValidateRangeInvalidTiming() {
		this.dates.validateDateTimeRange("20/06/2020", "20/06/2020", "11:00:00", "10:59:59", this.monitor);
	}

	@Test
	public void testValidateRangeNoFrom() {
		this.dates.validateDateRange(null, "21/06/2020", this.monitor);
		assertTrue(true);
	}

	@Test
	public void testValidateRangeNoTo() {
		this.dates.validateDateRange("20/06/2020", null, this.monitor);
		assertTrue(true);
	}

	@Test
	public void testValidateRange() {
		this.dates.validateDateRange("20/06/2020", "21/06/2020", this.monitor);
		assertTrue(true);
	}

	@Test
	public void testGetDateTime() {
		assertNotNull(this.dates.getDateTime("20/06/2020", null, this.monitor));
	}

	@Test
	public void testValidateRangeTiming() {
		this.dates.validateDateTimeRange("20/06/2020", "20/06/2020", "00:00:00", "23:59:59", this.monitor);
		assertTrue(true);
	}

	@Test
	public void testrangeOverlap1() {
		Date range1From = this.dates.getDateTime("20/06/2020", "00:00:00", this.monitor);
		Date range1To = this.dates.getDateTime("21/06/2020", "23:59:59", this.monitor);
		Date range2From = this.dates.getDateTime("22/06/2020", "00:00:00", this.monitor);
		Date range2To = this.dates.getDateTime("23/06/2020", "23:59:59", this.monitor);

		assertFalse(this.dates.rangeOverlap(range1From, range1To, range2From, range2To));
	}

	@Test
	public void testrangeOverlap2() {
		Date range1From = this.dates.getDateTime("22/06/2020", "00:00:00", this.monitor);
		Date range1To = this.dates.getDateTime("23/06/2020", "23:59:59", this.monitor);
		Date range2From = this.dates.getDateTime("20/06/2020", "00:00:00", this.monitor);
		Date range2To = this.dates.getDateTime("21/06/2020", "23:59:59", this.monitor);

		assertFalse(this.dates.rangeOverlap(range1From, range1To, range2From, range2To));
	}

	@Test
	public void testrangeOverlap3() {
		Date range1From = this.dates.getDateTime("20/06/2020", "00:00:00", this.monitor);
		Date range1To = this.dates.getDateTime("27/06/2020", "23:59:59", this.monitor);
		Date range2From = this.dates.getDateTime("27/06/2020", "00:00:00", this.monitor);
		Date range2To = this.dates.getDateTime("30/06/2020", "23:59:59", this.monitor);

		assertTrue(this.dates.rangeOverlap(range1From, range1To, range2From, range2To));
	}

	@Test
	public void testBetweenRange1() {
		Date range1From = this.dates.getDateTime("20/06/2020", "00:00:00", this.monitor);
		Date range1To = this.dates.getDateTime("27/06/2020", "23:59:59", this.monitor);
		Date currentDate = this.dates.getDateTime("24/06/2020", "00:00:00", this.monitor);

		assertTrue(this.dates.betweenRange(currentDate, range1From, range1To));
	}

	@Test
	public void testBetweenRange2() {
		Date range1From = this.dates.getDateTime("20/06/2020", "00:00:00", this.monitor);
		Date range1To = this.dates.getDateTime("27/06/2020", "23:59:59", this.monitor);
		Date currentDate = this.dates.getDateTime("30/06/2020", "00:00:00", this.monitor);

		assertFalse(this.dates.betweenRange(currentDate, range1From, range1To));
	}

	@Test
	public void testBetweenRange3() {
		Date range1From = this.dates.getDateTime("20/06/2020", "00:00:00", this.monitor);
		Date range1To = this.dates.getDateTime("27/06/2020", "23:59:59", this.monitor);
		Date currentDate = this.dates.getDateTime("17/06/2020", "00:00:00", this.monitor);

		assertFalse(this.dates.betweenRange(currentDate, range1From, range1To));
	}

	@Test
	public void testCurrentBetweenRange() {
		Date range1From = this.dates.getDateTime("20/06/2020", "00:00:00", this.monitor);
		Date range1To = this.dates.getDateTime("27/06/2020", "23:59:59", this.monitor);

		assertFalse(this.dates.currentBetweenRange(range1From, range1To));
	}
}
