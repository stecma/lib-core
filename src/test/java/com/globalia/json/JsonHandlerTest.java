package com.globalia.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class JsonHandlerTest {

	@InjectMocks
	private JsonHandler jsonHandler;
	@Mock
	private ObjectMapper mapper;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testFromJsonEmptyJson() throws IOException {
		assertNull(this.jsonHandler.fromJson("", Object.class));
	}

	@Test(expected = IOException.class)
	public void testFromJsonIOException() throws IOException {
		when(this.mapper.readValue("json", Object.class)).thenThrow(IOException.class);
		this.jsonHandler.fromJson("json", Object.class);
	}

	@Test
	public void testFromJson() throws IOException {
		when(this.mapper.readValue("json", Object.class)).thenReturn(new Object());
		assertNotNull(this.jsonHandler.fromJson("json", Object.class));
	}

	@Test
	public void testFromJsonTypeEmptyJson() throws IOException {
		assertNull(this.jsonHandler.fromJson("", new TypeReference<>() {
		}));
	}

	@Test(expected = IOException.class)
	public void testFromJsonTypeIOException() throws IOException {
		when(this.mapper.readValue(anyString(), (TypeReference<?>) any())).thenThrow(IOException.class);
		this.jsonHandler.fromJson("json", new TypeReference<>() {
		});
	}

	@Test
	public void testFromJsonType() throws IOException {
		when(this.mapper.readValue(anyString(), (TypeReference<?>) any())).thenReturn(new Object());
		assertNotNull(this.jsonHandler.fromJson("json", new TypeReference<>() {
		}));
	}

	@Test(expected = JsonProcessingException.class)
	public void testToJsonJsonProcessingException() throws JsonProcessingException {
		when(this.mapper.writeValueAsString(any())).thenThrow(JsonProcessingException.class);
		this.jsonHandler.toJson(new Object());
	}

	@Test
	public void testToJson() throws JsonProcessingException {
		when(this.mapper.writeValueAsString(any())).thenReturn("json");
		assertNotNull(this.jsonHandler.toJson(new Object()));
	}
}
