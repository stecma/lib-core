package com.globalia.redis;

import com.globalia.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RedisAccessTest {

	@InjectMocks
	private RedisAccess redisAccess;
	@Mock
	private Context context;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetRediskey() {
		when(this.context.getEnvironment()).thenReturn("local");
		assertEquals("local:key", this.redisAccess.getRediskey("key"));
	}
}
