package com.globalia.redis;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings({"unused", "unchecked"})
@RunWith(MockitoJUnitRunner.Silent.class)
public class RedisClientTest {

	private static final String KEY = "rediskey";

	@InjectMocks
	private RedisClient redisClient;
	@Mock
	private HashOperations hashOperations;
	@Mock
	private SetOperations setOperations;
	@Mock
	private RedisTemplate template;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@SuppressWarnings("ConfusingArgumentToVarargsMethod")
	@Test
	public void testInit() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
		Method postConstruct = RedisClient.class.getDeclaredMethod("init", null); // methodName,parameters
		postConstruct.setAccessible(true);
		postConstruct.invoke(this.redisClient);
		assertTrue(true);
	}

	@Test
	public void testGetNull() {
		when(this.hashOperations.get(anyString(), anyString())).thenReturn(null);
		Object result = this.redisClient.get(RedisClientTest.KEY, "id");
		assertNull(result);
	}

	@Test
	public void testGet() {
		when(this.hashOperations.get(anyString(), anyString())).thenReturn("test");
		Object result = this.redisClient.get(RedisClientTest.KEY, "id");
		assertEquals("test", result);
	}

	@Test
	public void testGetAll() {
		when(this.hashOperations.entries(anyString())).thenReturn(new HashMap<>());
		Map<String, Object> result = this.redisClient.getAll(RedisClientTest.KEY);
		assertNotNull(result);
	}

	@Test
	public void testMembers() {
		when(this.setOperations.members(anyString())).thenReturn(new HashSet<>());
		Set<String> result = this.redisClient.members(RedisClientTest.KEY);
		assertNotNull(result);
	}

	@Test
	public void testHasKey() {
		when(this.hashOperations.hasKey(anyString(), anyString())).thenReturn(true);
		assertTrue(this.redisClient.hasKey(RedisClientTest.KEY, "1"));
	}

	@Test
	public void testAddMapNull() {
		this.redisClient.addMap(RedisClientTest.KEY, null);
		assertTrue(true);
	}

	@Test
	public void testAddMap() {
		Map<String, Object> map = new HashMap<>();
		map.put("id", "json");
		this.redisClient.addMap(RedisClientTest.KEY, map);
		assertTrue(true);
	}

	@Test
	public void testAdd() {
		this.redisClient.add(RedisClientTest.KEY, "id", "json");
		assertTrue(true);
	}

	@Test
	public void testAddSet() {
		this.redisClient.add(RedisClientTest.KEY, "id");
		assertTrue(true);
	}

	@Test
	public void testAddListNullList() {
		this.redisClient.addList(RedisClientTest.KEY, null);
		assertTrue(true);
	}

	@Test
	public void testAddList() {
		this.redisClient.addList(RedisClientTest.KEY, new ArrayList<>(Collections.singletonList("id")));
		assertTrue(true);
	}

	@Test
	public void testDelete() {
		this.redisClient.delete(RedisClientTest.KEY, "id");
		assertTrue(true);
	}

	@Test
	public void testRemove() {
		this.redisClient.remove(RedisClientTest.KEY, "id");
		assertTrue(true);
	}

	@Test
	public void testKeys() {
		assertNotNull(this.redisClient.keys(RedisClientTest.KEY));
	}

	@Test
	public void testPatternKeys() {
		when(this.template.keys(anyString())).thenReturn(Set.of(RedisClientTest.KEY));
		assertNotNull(this.redisClient.patternKeys(RedisClientTest.KEY));
	}

	@Test
	public void testExists() {
		when(this.template.hasKey(anyString())).thenReturn(true);
		assertTrue(this.redisClient.existKey(RedisClientTest.KEY));
	}

	@Test
	public void testDeleteKey() {
		this.redisClient.deleteKey(RedisClientTest.KEY);
		assertTrue(true);
	}

	@Test
	public void testIsMember() {
		when(this.setOperations.isMember(anyString(), anyString())).thenReturn(true);
		assertTrue(this.redisClient.isMember(RedisClientTest.KEY, "id"));
	}

	@Test
	public void testRename() {
		this.redisClient.rename("old", "new");
		assertTrue(true);
	}

	@Test
	public void testScanNull() throws IOException {
		when(this.setOperations.scan(anyString(), any())).thenReturn(new Cursor<String>() {

			private boolean hasNext = false;

			@Override
			public long getCursorId() {
				return 0;
			}

			@Override
			public boolean isClosed() {
				return false;
			}

			@Override
			public Cursor<String> open() {
				return null;
			}

			@Override
			public long getPosition() {
				return 0;
			}

			@Override
			public void close() {

			}

			@Override
			public boolean hasNext() {
				return hasNext;
			}

			@Override
			public String next() {
				hasNext = false;
				return "test";
			}
		});
		assertTrue(this.redisClient.scan("test", "test", false).isEmpty());
	}

	@Test
	public void testScan() throws IOException {
		when(this.setOperations.scan(anyString(), any())).thenReturn(new Cursor<String>() {

			private boolean hasNext = true;

			@Override
			public long getCursorId() {
				return 0;
			}

			@Override
			public boolean isClosed() {
				return false;
			}

			@Override
			public Cursor<String> open() {
				return null;
			}

			@Override
			public long getPosition() {
				return 0;
			}

			@Override
			public void close() {

			}

			@Override
			public boolean hasNext() {
				return hasNext;
			}

			@Override
			public String next() {
				hasNext = false;
				return "test";
			}
		});
		assertNotNull(this.redisClient.scan("test", "test", false));
	}

	@Test
	public void testScanKeys() throws IOException {
		when(this.setOperations.scan(anyString(), any())).thenReturn(new Cursor<String>() {

			private boolean hasNext = true;

			@Override
			public long getCursorId() {
				return 0;
			}

			@Override
			public boolean isClosed() {
				return false;
			}

			@Override
			public Cursor<String> open() {
				return null;
			}

			@Override
			public long getPosition() {
				return 0;
			}

			@Override
			public void close() {

			}

			@Override
			public boolean hasNext() {
				return hasNext;
			}

			@Override
			public String next() {
				hasNext = false;
				return "[test] test";
			}
		});
		assertNotNull(this.redisClient.scanKeys("test", "test"));
	}
}