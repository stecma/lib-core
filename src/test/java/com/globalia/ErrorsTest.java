package com.globalia;

import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static org.junit.Assert.assertNotNull;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ErrorsTest {

	@InjectMocks
	private Errors errors;
	@Mock
	private MonitorHandler handler;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testAddError1() {
		assertNotNull(this.errors.addError(HttpStatus.ACCEPTED, "error", new Monitor(), "class", "method", 1));
	}

	@Test
	public void testAddError2() {
		assertNotNull(this.errors.addError("error", new Monitor(), "class", "method", 1));
	}

	@Test
	public void testAddError3() {
		assertNotNull(this.errors.addError("error", ErrorLayer.SERVICE_LAYER, new Monitor()));
	}
}
