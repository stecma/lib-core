package com.globalia.advice;

import com.globalia.api.ApiRS;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class HandlerAdviceTest {

	private static final String RELATION_TEST = "HandlerExceptionTest";
	private static final String SERVICE_TEST = "service";
	private static final String PORT_KEY = "port";

	@InjectMocks
	private HandlerAdvice handlerAdvice;
	@Mock
	private MonitorHandler handler;
	@Mock
	private HttpServletRequest request;

	private Monitor monitor;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
		this.monitor = new Monitor();
		this.monitor.setLogs(new HashSet<>());
		this.monitor.setStats(new HashSet<>());
	}

	@Test
	public void testHandleValidateException() {
		when(this.handler.create(nullable(String.class), anyString(), anyString())).thenReturn(this.monitor);
		ApiRS response = this.handlerAdvice.handleValidateException(this.request, new ValidateException(new Error("test", ErrorLayer.UNKNOWN_LAYER), this.monitor), HandlerAdviceTest.SERVICE_TEST, HandlerAdviceTest.PORT_KEY, LogType.DEBUG);
		assertNotNull(response);
		assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
	}

	@Test
	public void testHandleValidateExceptionNullMonitor() {
		when(this.handler.create(nullable(String.class), anyString(), anyString())).thenReturn(this.monitor);
		ApiRS response = this.handlerAdvice.handleValidateException(this.request, new ValidateException(new Error("test", ErrorLayer.UNKNOWN_LAYER), null), HandlerAdviceTest.SERVICE_TEST, HandlerAdviceTest.PORT_KEY, LogType.DEBUG);
		assertNotNull(response);
		assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
	}

	@Test
	public void testHandleException() {
		when(this.request.getRequestURI()).thenReturn(HandlerAdviceTest.RELATION_TEST);
		when(this.handler.create(nullable(String.class), anyString(), anyString())).thenReturn(this.monitor);

		ApiRS response = this.handlerAdvice.handleException(this.request, new Exception(), HandlerAdviceTest.SERVICE_TEST, HandlerAdviceTest.PORT_KEY, LogType.DEBUG);
		assertNotNull(response);
		assertEquals(response.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR.value());
	}
}
