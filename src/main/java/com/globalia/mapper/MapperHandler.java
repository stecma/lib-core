package com.globalia.mapper;

import com.globalia.aspect.Utils;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Utils
@Component
public class MapperHandler {

	@Autowired
	private MonitorHandler handler;
	@Autowired
	protected JsonHandler jsonHandler;

	/**
	 * Add error.
	 *
	 * @param msgErr     Message error.
	 * @param className  Class name.
	 * @param methodName Method name.
	 * @param lineNumber Line.
	 * @return Error.
	 */
	public Error addError(final String msgErr, final Monitor monitor, final String className, final String methodName, final int lineNumber) {
		this.handler.addErrorLog(monitor, className, methodName, lineNumber, msgErr, LogType.ERROR);
		return new Error(msgErr, ErrorLayer.REPOSITORY_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value());
	}
}