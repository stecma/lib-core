package com.globalia.dao;

import com.globalia.Context;
import com.globalia.json.JsonHandler;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstrac class defined to implement the API CRUD dao's.
 */
public abstract class BaseDao {

	@Getter
	@Autowired
	private JsonHandler jsonHandler;
	@Getter
	@Autowired
	private Context context;
}
