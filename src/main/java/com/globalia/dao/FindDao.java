package com.globalia.dao;

/**
 * Abstrac class defined to implement the API CRUD dao's to find data.
 */
public abstract class FindDao<T> extends BaseDao implements IFind<T> {
}
