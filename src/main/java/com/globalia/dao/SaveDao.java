package com.globalia.dao;

/**
 * Abstrac class defined to implement the API CRUD dao's to save data.
 */
public abstract class SaveDao<T> extends BaseDao implements ISave<T> {
}
