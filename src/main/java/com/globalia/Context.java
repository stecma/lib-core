package com.globalia;

import com.globalia.aspect.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;

@Utils
@Component
public class Context {

	@Autowired
	private Environment environment;

	public String getEnvironment() {
		String env = "dft";
		for (String profile : this.environment.getActiveProfiles()) {
			if (!StringUtils.hasText(profile) || "kubernetes".equalsIgnoreCase(profile)) {
				continue;
			}
			env = profile;
		}
		return env;
	}

	public String getEnvironments() {
		return Arrays.toString(this.environment.getActiveProfiles());
	}
}