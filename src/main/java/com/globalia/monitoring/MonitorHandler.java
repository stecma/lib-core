package com.globalia.monitoring;

import com.globalia.enumeration.LogType;
import com.globalia.enumeration.StatType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Component
public class MonitorHandler {

	/**
	 * Create monitor.
	 *
	 * @param request Request.
	 * @param service Service.
	 * @param port    Port.
	 * @return Monitor.
	 */
	public Monitor create(final String request, final String service, final String port) {
		Monitor monitor = new Monitor();
		monitor.setTimestamp(System.currentTimeMillis());
		monitor.setCorrelationID(UUID.randomUUID().toString());
		monitor.setService(service);
		monitor.setPort(port);
		monitor.setLogs(new HashSet<>());
		monitor.setStats(new HashSet<>());

		if (StringUtils.hasText(request)) {
			this.addLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), this.ofuscateRQ(request), LogType.INFO, LogType.NONE);
			this.addStat(monitor, StatType.INIT, 0L);
		}
		return monitor;
	}

	/**
	 * Add debug log.
	 *
	 * @param monitor         Monitor.
	 * @param clazz           ClassName.
	 * @param method          Method.
	 * @param line            Line number.
	 * @param message         Message.
	 * @param allowedLogLevel Log level allowed.
	 */
	public void addDebugLog(final Monitor monitor, final String clazz, final String method, final int line, final String message, final LogType allowedLogLevel) {
		this.addLog(monitor, clazz, method, line, message, LogType.DEBUG, allowedLogLevel);
	}

	/**
	 * Add info log.
	 *
	 * @param monitor         Monitor.
	 * @param clazz           ClassName.
	 * @param method          Method.
	 * @param line            Line number.
	 * @param message         Message.
	 * @param allowedLogLevel Log level allowed.
	 */
	public void addInfoLog(final Monitor monitor, final String clazz, final String method, final int line, final String message, final LogType allowedLogLevel) {
		this.addLog(monitor, clazz, method, line, message, LogType.INFO, allowedLogLevel);
	}

	/**
	 * Add warning log.
	 *
	 * @param monitor         Monitor.
	 * @param clazz           ClassName.
	 * @param method          Method.
	 * @param line            Line number.
	 * @param message         Message.
	 * @param allowedLogLevel Log level allowed.
	 */
	public void addWarnLog(final Monitor monitor, final String clazz, final String method, final int line, final String message, final LogType allowedLogLevel) {
		this.addLog(monitor, clazz, method, line, message, LogType.WARN, allowedLogLevel);
	}

	/**
	 * Add error log.
	 *
	 * @param monitor         Monitor.
	 * @param clazz           ClassName.
	 * @param method          Method.
	 * @param line            Line number.
	 * @param message         Message.
	 * @param allowedLogLevel Log level allowed.
	 */
	public void addErrorLog(final Monitor monitor, final String clazz, final String method, final int line, final String message, final LogType allowedLogLevel) {
		this.addLog(monitor, clazz, method, line, message, LogType.ERROR, allowedLogLevel);
	}

	/**
	 * Add stat.
	 *
	 * @param monitor Monitor.
	 * @param type    Stat ytype.
	 * @param elapsed Elapsed time.
	 */
	public void addStat(final Monitor monitor, final StatType type, final long elapsed) {
		if (monitor != null && monitor.getStats() != null) {
			Stat stat = new Stat();
			stat.setType(type);
			stat.setTimestamp(System.currentTimeMillis());
			stat.setService(monitor.getService());
			stat.setPort(monitor.getPort());
			stat.setHost(this.getHost());
			stat.setElapsedMS(elapsed);
			monitor.getStats().add(stat);
		}
	}

	/**
	 * Convert Monitor to Json.
	 *
	 * @param monitor Monitor.
	 * @return Json.
	 */
	public String toJson(final Monitor monitor) {
		String result = null;
		if (monitor != null) {
			StringBuilder json = new StringBuilder("{");
			json.append(String.format("\"times\":\"%s\", \"uid\":\"%s\", \"route\":\"%s\"", monitor.getTimestamp(), monitor.getCorrelationID(), monitor.getRoute()));

			StringBuilder subJson = this.addLogs(monitor);
			if (subJson != null) {
				json.append(", \"logs\": [").append(subJson.toString()).append("]");
			}
			subJson = this.addStats(monitor);
			if (subJson != null) {
				json.append(", \"stats\": [").append(subJson.toString()).append("]");
			}
			json.append("}");
			result = json.toString();
		}
		return result;
	}

	/**
	 * Get sorted logs.
	 *
	 * @param logs Logs.
	 * @return Sorted logs.
	 */
	public Set<Log> sortedLogs(final Set<Log> logs) {
		Set<Log> sortedLogs = null;
		if (logs != null) {
			List<Log> list = new ArrayList<>(logs);
			list.sort((log1, log2) -> (int) (log1.getTimestamp() - log2.getTimestamp()));

			sortedLogs = new LinkedHashSet<>(list);
		}
		return sortedLogs;
	}

	/**
	 * Get sorted stats.
	 *
	 * @param stats Stats.
	 * @return Sorted stats.
	 */
	public Set<Stat> sortedStats(final Set<Stat> stats) {
		Set<Stat> sortedStats = null;
		if (stats != null) {
			List<Stat> list = new ArrayList<>(stats);
			list.sort((stat1, stat2) -> (int) (stat1.getTimestamp() - stat2.getTimestamp()));

			sortedStats = new LinkedHashSet<>(list);
		}
		return sortedStats;
	}

	private StringBuilder addStats(final Monitor monitor) {
		StringBuilder json = null;
		if (monitor.getStats() != null) {
			for (Stat stat : monitor.getStats()) {
				if (json == null) {
					json = new StringBuilder();
				}
				if (json.length() > 1) {
					json.append(",");
				}
				json.append(String.format("{\"times\":\"%s\", \"host\":\"%s\", \"app\":\"%s\", \"type\":\"%S\", \"eld\":\"%S\"}", stat.getTimestamp(), stat.getHost(), stat.getService(), stat.getType().toString(), stat.getElapsedMS()));
			}
		}
		return json;
	}

	private StringBuilder addLogs(final Monitor monitor) {
		StringBuilder json = null;
		if (monitor.getLogs() != null) {
			for (Log log : monitor.getLogs()) {
				if (json == null) {
					json = new StringBuilder();
				}
				if (json.length() > 1) {
					json.append(",");
				}
				json.append(String.format("{\"times\":\"%s\", \"type\":\"%s\", \"app\":\"%s\", \"host\":\"%s\", \"cls\":\"%s\", \"mtd\":\"%s\", \"line\":%s, \"msg\":\"%s\"}", log.getTimestamp(), log.getType().toString(), log.getService(), log.getHost(), log.getClazz(), log.getMethod(), log.getLine(), log.getMessage()));
			}
		}
		return json;
	}

	private void addLog(final Monitor monitor, final String clazz, final String method, final int line, final String message, final LogType type, final LogType allowedLogType) {
		if (monitor != null && monitor.getLogs() != null && this.isAllowed(type, allowedLogType)) {
			Log log = new Log();
			log.setClazz(clazz);
			log.setMethod(method);
			log.setLine(line);
			log.setTimestamp(System.currentTimeMillis());
			log.setMessage(message.replace("\"", "'").replace("\n", ""));
			log.setType(type);
			log.setService(monitor.getService());
			log.setPort(monitor.getPort());
			log.setHost(this.getHost());
			monitor.getLogs().add(log);
		}
	}

	private String getHost() {
		String host = null;
		try {
			host = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException ignored) {
			//
		}
		return host;
	}

	private boolean isAllowed(final LogType logType, final LogType allowedLogType) {
		boolean result = false;
		if (allowedLogType == LogType.NONE || logType.ordinal() <= allowedLogType.ordinal()) {
			result = true;
		}
		return result;
	}

	private String ofuscateRQ(final String request) {
		if (request.contains("pass")) {
			String ofuscate = "";
			String[] splits = request.split(",");
			for (String param : splits) {
				if (param.contains("pass")) {
					ofuscate = param.split(":")[1].replaceAll("\"", "");
					break;
				}
			}
			return request.replace(ofuscate, "*".repeat(ofuscate.length()));
		}
		return request;
	}
}