package com.globalia.security;

import com.globalia.enumeration.LogType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

@Component
public class JwtTokenFilterConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

	@Autowired
	private JwtTokenFilter tokenFilter;

	public void init(final LogType logLevel, final String jwtSecret, final String encryptSecret, final String encryptAlgorithm, final String service, final String port) {
		this.tokenFilter.init(logLevel, jwtSecret, encryptSecret, encryptAlgorithm, service, port);
	}

	@Override
	public void configure(final HttpSecurity http) {
		http.addFilterBefore(this.tokenFilter, UsernamePasswordAuthenticationFilter.class);
	}
}