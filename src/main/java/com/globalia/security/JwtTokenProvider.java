package com.globalia.security;

import com.globalia.Context;
import com.globalia.configuration.TokenConfig;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.dto.login.UserItem;
import com.globalia.dto.login.UserMenu;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {

	static final String AUTHORIZATION = "Authorization";
	private static final String ADMIN_KEY = "adm";
	private static final String ENV_KEY = "env";
	private static final String SHA_KEY = "sha";
	private static final String SERVICE_KEY = "srv";
	private static final String BEARER = "Bearer ";
	private static final String JWT_PARSE_ERR = "JWT parser error %s";
	private static final String NOT_ALLOWED_METHOD = "PUT,DELETE,POST";
	private static final String INVALID_JWT_CONFIG = "Invalid JWT configuration";
	private static final String DENIED_ACCESS = "Denied Access";
	private static final String INIT_PROCESS = "Start process";
	private static final String END_PROCESS = "End process";

	@Getter
	@Autowired
	private MonitorHandler handler;
	@Getter
	@Autowired
	private AESPassEncrypter encrypter;
	@Autowired
	private Context context;

	/**
	 * Create new JWT token.
	 *
	 * @param user        User details.
	 * @param tokenConfig Token configuration.
	 * @param monitor     Monitor handler.
	 * @return Token.
	 */
	public String createToken(final UserItem user, final TokenConfig tokenConfig, final Monitor monitor) {
		if (user == null || !StringUtils.hasText(user.getId()) || !StringUtils.hasText(user.getPass())) {
			throw new ValidateException(new Error("Invalid credentials", ErrorLayer.AUTH_LAYER), monitor);
		}
		if (tokenConfig == null || !StringUtils.hasText(tokenConfig.getSecret()) || tokenConfig.getExpirationAdminInMs() == 0 || tokenConfig.getExpirationInMs() == 0) {
			throw new ValidateException(new Error(JwtTokenProvider.INVALID_JWT_CONFIG, ErrorLayer.AUTH_LAYER), monitor);
		}
		return this.tokenGeneration(user, tokenConfig, monitor);
	}

	/**
	 * Get user from token.
	 *
	 * @param token       Token.
	 * @param tokenConfig Token configuration.
	 * @param monitor     Monitor handler.
	 * @return User.
	 */
	public String getUser(final String token, final TokenConfig tokenConfig, final Monitor monitor) {
		if (tokenConfig == null || !StringUtils.hasText(tokenConfig.getSecret())) {
			throw new ValidateException(new Error(JwtTokenProvider.INVALID_JWT_CONFIG, ErrorLayer.AUTH_LAYER), monitor);
		}

		String userName;
		try {
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.INIT_PROCESS, tokenConfig.getAllowedLogLevel());
			Claims claims = this.parser(token, tokenConfig.getSecret()).getBody();
			userName = claims.getSubject();
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.END_PROCESS, tokenConfig.getAllowedLogLevel());
		} catch (ServletException sve) {
			throw new ValidateException(new Error(String.format(JwtTokenProvider.JWT_PARSE_ERR, sve.getMessage()), ErrorLayer.AUTH_LAYER), monitor);
		}
		return userName;
	}

	/**
	 * Refresh token.
	 *
	 * @param token       JWT token.
	 * @param user        User details.
	 * @param tokenConfig Token configuration.
	 * @param monitor     Monitor handler.
	 * @return Token.
	 */
	public String refreshToken(final String token, final UserItem user, final TokenConfig tokenConfig, final Monitor monitor) {
		if (tokenConfig == null || !StringUtils.hasText(tokenConfig.getSecret()) || tokenConfig.getExpirationAdminInMs() == 0 || tokenConfig.getExpirationInMs() == 0) {
			throw new ValidateException(new Error(JwtTokenProvider.INVALID_JWT_CONFIG, ErrorLayer.AUTH_LAYER), monitor);
		}

		this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.INIT_PROCESS, tokenConfig.getAllowedLogLevel());
		String newToken;
		try {
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Parser Token", tokenConfig.getAllowedLogLevel());
			this.parser(token, tokenConfig.getSecret()).getBody();
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Generate Token", tokenConfig.getAllowedLogLevel());
			newToken = this.tokenGeneration(user, tokenConfig, monitor);
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Done", tokenConfig.getAllowedLogLevel());
		} catch (ServletException sve) {
			throw new ValidateException(new Error(String.format(JwtTokenProvider.JWT_PARSE_ERR, sve.getMessage()), ErrorLayer.AUTH_LAYER), monitor);
		}
		this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Fin Proceso", tokenConfig.getAllowedLogLevel());
		return newToken;
	}

	/**
	 * Refresh expiration.
	 *
	 * @param token       JWT token.
	 * @param tokenConfig Token configuration.
	 * @param monitor     Monitor handler.
	 * @return Token.
	 */
	public String refreshExpiration(final String token, final TokenConfig tokenConfig, final Monitor monitor) {
		if (tokenConfig == null || !StringUtils.hasText(tokenConfig.getSecret()) || tokenConfig.getExpirationAdminInMs() == 0 || tokenConfig.getExpirationInMs() == 0) {
			throw new ValidateException(new Error(JwtTokenProvider.INVALID_JWT_CONFIG, ErrorLayer.PROXY_LAYER), monitor);
		}

		this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.INIT_PROCESS, tokenConfig.getAllowedLogLevel());
		String newToken;
		try {
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Parser Token", tokenConfig.getAllowedLogLevel());
			Claims claims = this.parser(this.resolveToken(token), tokenConfig.getSecret()).getBody();
			int expiredMs = tokenConfig.getExpirationAdminInMs();
			if (!(boolean) claims.get(JwtTokenProvider.ADMIN_KEY)) {
				expiredMs = tokenConfig.getExpirationInMs();
			}

			claims.remove(Claims.EXPIRATION);
			newToken = JwtTokenProvider.BEARER + Jwts.builder().setClaims(claims).setExpiration(new Date(new Date().getTime() + expiredMs)).signWith(SignatureAlgorithm.HS256, tokenConfig.getSecret()).compact();
		} catch (ServletException sve) {
			throw new ValidateException(new Error(String.format(JwtTokenProvider.JWT_PARSE_ERR, sve.getMessage()), ErrorLayer.PROXY_LAYER), monitor);
		} finally {
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Fin Proceso", tokenConfig.getAllowedLogLevel());
		}
		return newToken;
	}

	/**
	 * Token validation.
	 *
	 * @param token       Token.
	 * @param tokenConfig Token configuration.
	 * @param request     HTTP request.
	 * @param monitor     Monitor handler.
	 * @throws ServletException Possible exception.
	 */
	@SuppressWarnings("unchecked")
	public void tokenValidation(final String token, final HttpServletRequest request, final TokenConfig tokenConfig, final Monitor monitor) throws ServletException {
		if (tokenConfig == null || !StringUtils.hasText(tokenConfig.getSecret())) {
			throw new ValidateException(new Error(JwtTokenProvider.INVALID_JWT_CONFIG, ErrorLayer.AUTH_LAYER), monitor);
		}

		this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.INIT_PROCESS, tokenConfig.getAllowedLogLevel());
		Claims claims = this.parser(token, tokenConfig.getSecret()).getBody();
		this.environmentValidation((String) claims.get(JwtTokenProvider.ENV_KEY));

		if ((boolean) claims.get(JwtTokenProvider.ADMIN_KEY)) {
			return;
		}

		Map<String, Boolean> uriMap = (Map<String, Boolean>) claims.get(JwtTokenProvider.SERVICE_KEY);
		if (uriMap.isEmpty()) {
			throw new ServletException(JwtTokenProvider.DENIED_ACCESS);
		}

		if (!this.isValidUser(uriMap, request)) {
			throw new ServletException(JwtTokenProvider.DENIED_ACCESS);
		}
		this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.END_PROCESS, tokenConfig.getAllowedLogLevel());
	}

	/**
	 * Get authentication.
	 *
	 * @param token       Token.
	 * @param tokenConfig Token configuration.
	 * @param monitor     Monitor handler.
	 * @return Authentication.
	 * @throws ServletException Possible exception.
	 */
	public Authentication getAuthentication(final String token, final TokenConfig tokenConfig, final Monitor monitor) throws ServletException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		if (tokenConfig == null || !StringUtils.hasText(tokenConfig.getSecret()) || !StringUtils.hasText(tokenConfig.getSecretEncrypt())) {
			throw new ValidateException(new Error(JwtTokenProvider.INVALID_JWT_CONFIG, ErrorLayer.AUTH_LAYER), monitor);
		}

		Claims claims = this.parser(token, tokenConfig.getSecret()).getBody();
		return new UsernamePasswordAuthenticationToken(claims.getSubject(), this.encrypter.decrypt((String) claims.get(JwtTokenProvider.SHA_KEY), tokenConfig.getSecretEncrypt(), tokenConfig.getEncryptAlgorithm(), monitor, tokenConfig.getAllowedLogLevel()));
	}

	private String tokenGeneration(final UserItem user, final TokenConfig tokenConfig, final Monitor monitor) {
		this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.INIT_PROCESS, tokenConfig.getAllowedLogLevel());
		int expiredMs = tokenConfig.getExpirationAdminInMs();
		if (!user.isAdmin()) {
			expiredMs = tokenConfig.getExpirationInMs();
		}

		Date now = new Date();
		Date validity = new Date(now.getTime() + expiredMs);

		String token = Jwts.builder().setClaims(this.buildClaims(user, monitor, tokenConfig)).setIssuedAt(now).setExpiration(validity).signWith(SignatureAlgorithm.HS256, tokenConfig.getSecret()).compact();
		this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.END_PROCESS, tokenConfig.getAllowedLogLevel());
		return JwtTokenProvider.BEARER + token;
	}

	private Jws<Claims> parser(final String token, final String secret) throws ServletException {
		try {
			return Jwts.parser().setSigningKey(secret).parseClaimsJws(this.resolveToken(token));
		} catch (SignatureException ex) {
			throw new ServletException("Invalid JWT signature");
		} catch (MalformedJwtException | UnsupportedJwtException ex) {
			throw new ServletException("Invalid JWT token");
		} catch (ExpiredJwtException ex) {
			throw new ServletException("Expired JWT token");
		} catch (IllegalArgumentException ex) {
			throw new ServletException("JWT claims string is empty");
		}
	}

	private String resolveToken(final String token) {
		String result = token;
		if (StringUtils.hasText(result)) {
			return token.replace(JwtTokenProvider.BEARER, "");
		}
		return token;
	}

	private Claims buildClaims(final UserItem user, final Monitor monitor, final TokenConfig tokenConfig) {
		this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.INIT_PROCESS, tokenConfig.getAllowedLogLevel());
		Claims claims = Jwts.claims().setSubject(user.getId());
		claims.put(JwtTokenProvider.SHA_KEY, user.getPass());
		claims.put(JwtTokenProvider.ADMIN_KEY, user.isAdmin());
		claims.put(JwtTokenProvider.ENV_KEY, this.context.getEnvironment());
		if (!user.isAdmin()) {
			claims.put(JwtTokenProvider.SERVICE_KEY, this.getServices(user, tokenConfig.getForcedUrl()));
		}
		this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), JwtTokenProvider.INIT_PROCESS, tokenConfig.getAllowedLogLevel());
		return claims;
	}

	private Map<String, Boolean> getServices(final UserItem user, final String forcedUrl) {
		// Forced for the use of all users
		Map<String, Boolean> uriMap = this.setForcedUrl(forcedUrl);
		if (user.getConfiguration() != null && !user.getConfiguration().isEmpty()) {
			for (UserMenu config : user.getConfiguration()) {
				if (config.getMenus() != null) {
					for (MenuItem menu : config.getMenus()) {
						this.getServices(uriMap, menu.getServices());
						this.getGroups(uriMap, menu.getGroups());
					}
				}
			}
		}
		return uriMap;
	}

	private Map<String, Boolean> setForcedUrl(final String forcedUrl) {
		Map<String, Boolean> map = new HashMap<>();
		if (StringUtils.hasText(forcedUrl)) {
			JsonObject object = (JsonObject) new JsonParser().parse(forcedUrl);
			for (JsonElement route : object.get("routes").getAsJsonArray()) {
				map.putIfAbsent(route.getAsJsonObject().get("uri").getAsString(), route.getAsJsonObject().get("readOnly").getAsBoolean());
			}
		}
		return map;
	}

	private void getGroups(final Map<String, Boolean> uriMap, final Set<GroupItem> groups) {
		if (groups != null && !groups.isEmpty()) {
			for (GroupItem group : groups) {
				this.getServices(uriMap, group.getServices());
			}
		}
	}

	private void getServices(final Map<String, Boolean> uriMap, final Set<ServiceItem> services) {
		if (services != null && !services.isEmpty()) {
			for (ServiceItem service : services) {
				if (!service.isExternal()) {
					uriMap.putIfAbsent(service.getUrl().getRoute(), service.getUrl().isReadOnly());
				}
			}
		}
	}

	private boolean isValidUser(final Map<String, Boolean> uriMap, final HttpServletRequest request) {
		return !uriMap.keySet().stream().filter(u -> request.getRequestURI().contains(u) && this.hasPermission(uriMap.get(u), request)).collect(Collectors.toSet()).isEmpty();
	}

	private boolean hasPermission(final boolean readOnly, final HttpServletRequest request) {
		// Allowed if the user write permission for that service
		boolean hasPermission = !readOnly;

		// Allowed if the user has read permission and the http method isn't in the not allowed method list (Write methods)
		if (!hasPermission && !JwtTokenProvider.NOT_ALLOWED_METHOD.contains(request.getMethod())) {
			hasPermission = true;
		}

		// Allowed if the user has read permission and the POST method is used because searches are filtered
		if (!hasPermission && request.getMethod().equals("POST") && !request.getRequestURI().contains("add")) {
			hasPermission = true;
		}
		return hasPermission;
	}

	private void environmentValidation(final String enviroment) throws ServletException {
		if (!StringUtils.hasText(enviroment) || !this.context.getEnvironment().equals(enviroment)) {
			throw new ServletException(JwtTokenProvider.DENIED_ACCESS);
		}
	}
}