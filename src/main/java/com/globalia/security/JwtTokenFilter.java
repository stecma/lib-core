package com.globalia.security;

import com.globalia.api.ApiRS;
import com.globalia.configuration.TokenConfig;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.monitoring.Monitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Component
public class JwtTokenFilter extends OncePerRequestFilter {

	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	@Autowired
	private JsonHandler jsonHandler;

	private String jwtSecret;
	private String encryptSecret;
	private String encryptAlgorithm;
	private LogType logLevel;
	private String service;
	private String port;

	public void init(final LogType logType, final String jwtMySecret, final String encryptMySecret, final String encryptAlgorithm, final String serviceName, final String portNumber) {
		this.logLevel = logType;
		this.jwtSecret = jwtMySecret;
		this.encryptSecret = encryptMySecret;
		this.encryptAlgorithm = encryptAlgorithm;
		this.service = serviceName;
		this.port = portNumber;
	}

	@Override
	protected void doFilterInternal(final HttpServletRequest httpRQ, final HttpServletResponse httpRS, final FilterChain filter) throws IOException {
		try {
			String token = httpRQ.getHeader(JwtTokenProvider.AUTHORIZATION);
			if (StringUtils.hasText(token)) {
				TokenConfig tokenConfig = new TokenConfig();
				tokenConfig.setSecret(Base64.getEncoder().encodeToString(this.jwtSecret.getBytes()));
				tokenConfig.setAllowedLogLevel(this.logLevel);
				tokenConfig.setSecretEncrypt(this.encryptSecret);
				tokenConfig.setEncryptAlgorithm(this.encryptAlgorithm);
				this.jwtTokenProvider.tokenValidation(token, httpRQ, tokenConfig, null);

				//setting auth in the context.
				SecurityContextHolder.getContext().setAuthentication(this.jwtTokenProvider.getAuthentication(token, tokenConfig, null));
			}

			filter.doFilter(httpRQ, httpRS);
		} catch (ServletException | NullPointerException | IllegalBlockSizeException | InvalidKeyException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
			Error err = new Error(ex.getMessage(), ErrorLayer.AUTH_LAYER);
			err.setUri(httpRQ.getRequestURI());

			ApiRS response = new ApiRS();
			response.setResponse(err);
			Monitor monitor = this.jwtTokenProvider.getHandler().create(null, this.service, this.port);
			this.jwtTokenProvider.getHandler().addErrorLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), ex.getMessage(), this.logLevel);
			monitor.setEndDate(System.currentTimeMillis());

			response.setMonitor(monitor);
			response.setStatus(HttpStatus.UNAUTHORIZED.value());

			httpRS.setStatus(HttpStatus.UNAUTHORIZED.value());
			httpRS.getWriter().write(this.jsonHandler.toJson(response));
		}
	}
}