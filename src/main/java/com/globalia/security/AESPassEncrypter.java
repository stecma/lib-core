package com.globalia.security;

import com.globalia.enumeration.LogType;
import com.globalia.enumeration.StatType;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Component
public class AESPassEncrypter {

	@Autowired
	private MonitorHandler handler;

	/**
	 * Encrypt a string with AES algorithm.
	 *
	 * @param data            Data to encrypt.
	 * @param secret          secret key value.
	 * @param algorithm       Algorithm.
	 * @param monitor         Monitor object.
	 * @param allowedLogLevel Log level allowed.
	 * @return encrypted data.
	 */
	public String encrypt(final String data, final String secret, final String algorithm, final Monitor monitor, final LogType allowedLogLevel) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Cipher cipher;
		try {
			StopWatch watch = new StopWatch();
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Inicio proceso", allowedLogLevel);
			cipher = Cipher.getInstance(algorithm);
			watch.start();
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(secret.getBytes(), algorithm));
			String result = Base64.getEncoder().encodeToString(cipher.doFinal(data.getBytes()));
			watch.stop();
			this.handler.addStat(monitor, StatType.ENCRYPT, watch.getTotalTimeMillis());
			return result;
		} finally {
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Fin proceso", allowedLogLevel);
		}
	}

	/**
	 * Encrypt a string with AES algorithm.
	 *
	 * @param encryptedData   Data to decrypt.
	 * @param secret          secret key value.
	 * @param algorithm       Algorithm.
	 * @param monitor         Monitor object.
	 * @param allowedLogLevel Log level allowed.
	 * @return decrypted data.
	 */
	public String decrypt(final String encryptedData, final String secret, final String algorithm, final Monitor monitor, final LogType allowedLogLevel) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Cipher cipher;
		try {
			StopWatch watch = new StopWatch();
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Inicio proceso", allowedLogLevel);
			cipher = Cipher.getInstance(algorithm);
			watch.start();
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(secret.getBytes(), algorithm));
			String result = new String(cipher.doFinal(Base64.getDecoder().decode(encryptedData)));
			watch.stop();
			this.handler.addStat(monitor, StatType.DECRYPT, watch.getTotalTimeMillis());

			return result;
		} finally {
			this.handler.addDebugLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), "Fin proceso", allowedLogLevel);
		}
	}
}