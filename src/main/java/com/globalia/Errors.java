package com.globalia;

import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class Errors {

	public static final String EXCEPTION_MSG = "Unknown error during redis connection %s";

	@Autowired
	private MonitorHandler handler;

	/**
	 * Add error.
	 *
	 * @param msgErr  Message error.
	 * @param layer   Error layer.
	 * @param monitor Monitor.
	 * @return Error.
	 */
	public Error addError(final String msgErr, final ErrorLayer layer, final Monitor monitor) {
		this.addLog(msgErr, monitor, "", "", null);
		return new Error(msgErr, layer, HttpStatus.NO_CONTENT.value());
	}

	/**
	 * Add error.
	 *
	 * @param msgErr     Message error.
	 * @param monitor    Monitor.
	 * @param className  Class name.
	 * @param methodName Method name.
	 * @param lineNumber Line.
	 * @return Error.
	 */
	public Error addError(final String msgErr, final Monitor monitor, final String className, final String methodName, final Integer lineNumber) {
		this.addLog(msgErr, monitor, className, methodName, lineNumber);
		return new Error(msgErr, ErrorLayer.SERVICE_LAYER, HttpStatus.NO_CONTENT.value());
	}

	/**
	 * Add error.
	 *
	 * @param status     Status hppt.
	 * @param msgErr     Message error.
	 * @param monitor    Monitor.
	 * @param className  Class name.
	 * @param methodName Method name.
	 * @param lineNumber Line.
	 * @return Error.
	 */
	public Error addError(final HttpStatus status, final String msgErr, final Monitor monitor, final String className, final String methodName, final Integer lineNumber) {
		this.addLog(msgErr, monitor, className, methodName, lineNumber);
		return new Error(msgErr, ErrorLayer.REPOSITORY_LAYER, status.value());
	}

	private void addLog(final String msgErr, final Monitor monitor, final String className, final String methodName, final Integer lineNumber) {
		this.handler.addErrorLog(monitor, className, methodName, lineNumber != null ? lineNumber : 0, msgErr, LogType.ERROR);
	}
}
