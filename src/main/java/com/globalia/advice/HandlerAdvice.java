package com.globalia.advice;

import com.globalia.api.ApiRS;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

@Component
public class HandlerAdvice {

	private static final String FORMAT_PATTERN = "%s-%s";

	@Autowired
	private MonitorHandler handler;

	/**
	 * Validation handler exception.
	 *
	 * @param request   Request.
	 * @param exception ResourceNotFoundException.
	 * @param service   App name.
	 * @param port      Port.
	 * @param logLevel  Allowed Log level.
	 * @return Api response.
	 */
	public ApiRS handleValidateException(final HttpServletRequest request, final ValidateException exception, final String service, final String port, final LogType logLevel) {
		Error error = this.getError(exception.getError(), request.getRequestURI());
		return this.responseBuilder(error, this.getMonitor(exception.getMonitor(), service, port, logLevel, error.getErrorLayer(), error.getMessages()), HttpStatus.BAD_REQUEST);
	}

	/**
	 * Handler exception.
	 *
	 * @param request   Request.
	 * @param exception Exception.
	 * @param service   App name.
	 * @param port      Port.
	 * @param logLevel  Allowed Log level.
	 * @return Api response.
	 */
	public ApiRS handleException(final HttpServletRequest request, final Exception exception, final String service, final String port, final LogType logLevel) {
		Error err = new Error(exception.getMessage(), ErrorLayer.UNKNOWN_LAYER);
		err.setUri(request.getRequestURI());

		Monitor monitor = this.handler.create(null, service, port);
		this.handler.addErrorLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(HandlerAdvice.FORMAT_PATTERN, err.getErrorLayer(), err.getMessages().toString()), logLevel);
		monitor.setEndDate(System.currentTimeMillis());

		return this.responseBuilder(err, monitor, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private Error getError(final Error error, final String uri) {
		error.setUri(uri);
		return error;
	}

	private Monitor getMonitor(Monitor monitor, final String service, final String port, final LogType logLevel, final ErrorLayer layer, final Set<String> messages) {
		if (monitor == null) {
			monitor = this.handler.create(null, service, port);
		}
		monitor.setEndDate(System.currentTimeMillis());

		this.handler.addErrorLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), String.format(HandlerAdvice.FORMAT_PATTERN, layer, messages.toString()), logLevel);
		return monitor;
	}

	private ApiRS responseBuilder(final Object obj, final Monitor monitor, final HttpStatus status) {
		ApiRS response = new ApiRS();
		response.setResponse(obj);
		response.setMonitor(monitor);
		response.setStatus(status.value());
		return response;
	}
}