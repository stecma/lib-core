package com.globalia;

import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.monitoring.Monitor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Dates {

	private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public void validateDateRange(final String dateFrom, final String dateTo, final Monitor monitor) {
		this.validateDateTimeRange(dateFrom, dateTo, null, null, monitor);
	}

	public void validateDateTimeRange(final String dateFrom, final String dateTo, final String timeFrom, final String timeTo, final Monitor monitor) {
		if (StringUtils.hasText(dateFrom)) {
			Date from = this.getDateTime(dateFrom, (StringUtils.hasText(timeFrom) ? timeFrom : "00:00:00"), monitor);
			if (StringUtils.hasText(dateTo)) {
				Date until = this.getDateTime(dateTo, (StringUtils.hasText(timeTo) ? timeTo : "23:59:59"), monitor);
				if (until.before(from)) {
					throw new ValidateException(new Error("Date to is greater than the date from", ErrorLayer.API_LAYER), monitor);
				}
			}
		}
	}

	public Date getDateTime(final String date, final String time, final Monitor monitor) {
		try {
			this.formatter.setLenient(false);
			return this.formatter.parse(String.format("%s %s", date, (StringUtils.hasText(time) ? time : "00:00:00")));
		} catch (ParseException e) {
			throw new ValidateException(new Error("Incorrect format date", ErrorLayer.API_LAYER), monitor);
		}
	}

	public boolean rangeOverlap(final Date range1From, final Date range1To, final Date range2From, final Date range2To) {
		return range2To.after(range1From) && range2From.before(range1To);
	}

	public boolean currentBetweenRange(final Date dateFrom, final Date dateTo) {
		return this.betweenRange(new Date(), dateFrom, dateTo);
	}

	public boolean betweenRange(final Date currentDate, final Date dateFrom, final Date dateTo) {
		return currentDate.after(dateFrom) && currentDate.before(dateTo);
	}
}