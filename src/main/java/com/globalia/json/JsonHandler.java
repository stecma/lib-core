package com.globalia.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.globalia.aspect.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Utils
@Component
public class JsonHandler {

	@Autowired
	private ObjectMapper mapper;

	/**
	 * Convert value to Object.
	 *
	 * @param json    Json.
	 * @param classes Object result.
	 * @return Object.
	 */
	public Object fromJson(final String json, final Class<?> classes) throws IOException {
		if (StringUtils.hasText(json)) {
			return this.mapper.readValue(json, classes);
		}
		return null;
	}

	/**
	 * Convert value to Object.
	 *
	 * @param json    Json.
	 * @param typeRef Type reference.
	 * @return Object.
	 */
	public Object fromJson(final String json, final TypeReference<?> typeRef) throws IOException {
		if (StringUtils.hasText(json)) {
			return this.mapper.readValue(json, typeRef);
		}
		return null;
	}

	/**
	 * Convert object to value.
	 *
	 * @param obj Object to convert.
	 * @return value.
	 */
	public String toJson(final Object obj) throws JsonProcessingException {
		return this.mapper.writeValueAsString(obj);
	}
}