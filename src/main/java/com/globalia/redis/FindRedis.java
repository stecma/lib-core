package com.globalia.redis;

/**
 * Abstrac class defined to implement access to redis repository to find items.
 *
 * @param <T> Generic Type.
 */
public abstract class FindRedis<T> extends BaseRedis implements IFind<T> {
}