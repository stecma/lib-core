package com.globalia.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"unchecked", "rawtypes"})
@Component
public class RedisClient {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	private HashOperations hashOperations;
	private SetOperations setOperations;

	@PostConstruct
	private void init() {
		this.hashOperations = this.redisTemplate.opsForHash();
		this.setOperations = this.redisTemplate.opsForSet();
	}

	/**
	 * Get hash key value.
	 *
	 * @param redisKey Redis key.
	 * @param key      Key.
	 * @return Value Key.
	 */
	public Object get(final String redisKey, final String key) {
		return this.hashOperations.get(redisKey, key);
	}

	/**
	 * Get all values.
	 *
	 * @param redisKey Redis key.
	 * @return All values of the rediskey.
	 */
	public Map<String, Object> getAll(final String redisKey) {
		return this.hashOperations.entries(redisKey);
	}

	/**
	 * Add new hash value.
	 *
	 * @param redisKey Redis Key.
	 * @param id       Object identifier.
	 * @param object   Objeto.
	 */
	public void add(final String redisKey, final String id, final Object object) {
		this.hashOperations.put(redisKey, id, object);
	}

	/**
	 * Add new hashMap values.
	 *
	 * @param redisKey  Redis Key.
	 * @param objectMap Mapa de objetos.
	 */
	public void addMap(final String redisKey, final Map<String, Object> objectMap) {
		if (objectMap != null) {
			for (Map.Entry entry : objectMap.entrySet()) {
				this.hashOperations.put(redisKey, entry.getKey(), entry.getValue());
			}
		}
	}

	/**
	 * Delete hash key.
	 *
	 * @param redisKey Redis key.
	 * @param key      Key.
	 */
	public void delete(final String redisKey, final String key) {
		this.hashOperations.delete(redisKey, key);
	}

	/**
	 * Delete hash key.
	 *
	 * @param redisKey Redis key.
	 * @param key      Key.
	 */
	public Boolean hasKey(final String redisKey, final String key) {
		return this.hashOperations.hasKey(redisKey, key);
	}

	/**
	 * Get all keys.
	 *
	 * @param redisKey Redis key.
	 * @return All keys.
	 */
	public Set<String> keys(final String redisKey) {
		return this.hashOperations.keys(redisKey);
	}

	/**
	 * Get all values.
	 *
	 * @param redisKey Redis key.
	 * @return All values of the rediskey.
	 */
	public Set<String> members(final String redisKey) {
		return this.setOperations.members(redisKey);
	}

	/**
	 * Add new set value.
	 *
	 * @param redisKey Redis Key.
	 * @param id       Object identifier.
	 */
	public void add(final String redisKey, final String id) {
		this.setOperations.add(redisKey, id);
	}

	/**
	 * Add new value list.
	 *
	 * @param redisKey   Redis Key.
	 * @param objectList Object list.
	 */
	public void addList(final String redisKey, final List<Object> objectList) {
		if (objectList != null) {
			for (Object obj : objectList) {
				this.setOperations.add(redisKey, obj);
			}
		}
	}

	/**
	 * Remove set key.
	 *
	 * @param redisKey Redis key.
	 * @param key      Key.
	 */
	public void remove(final String redisKey, final String key) {
		this.setOperations.remove(redisKey, key);
	}

	/**
	 * Validate key.
	 *
	 * @param redisKey Redis key.
	 * @param key      Key.
	 * @return If the key exist or not.
	 */
	public Boolean isMember(final String redisKey, final String key) {
		return this.setOperations.isMember(redisKey, key);
	}

	/**
	 * Get all keys.
	 *
	 * @param redisKey Redis key.
	 * @return All keys.
	 */
	public Set<String> patternKeys(final String redisKey) {
		return this.redisTemplate.keys(redisKey);
	}

	/**
	 * Validate key.
	 *
	 * @param redisKey Redis key.
	 * @return If the key exist.
	 */
	public Boolean existKey(final String redisKey) {
		return this.redisTemplate.hasKey(redisKey);
	}

	/**
	 * Delete key.
	 *
	 * @param redisKey Redis key.
	 */
	public void deleteKey(final String redisKey) {
		this.redisTemplate.delete(redisKey);
	}

	/**
	 * Rename redis key.
	 *
	 * @param oldKey Old redis key.
	 * @param newKey New redis key.
	 */
	public void rename(final String oldKey, final String newKey) {
		this.redisTemplate.rename(oldKey, newKey);
	}

	/**
	 * Scan redis key.
	 *
	 * @param redisKey Redis key.
	 * @param pattern  Scan pattern.
	 */
	public Set<String> scan(final String redisKey, final String pattern, final boolean onlyKey) throws IOException {
		ScanOptions scanBuild = ScanOptions.scanOptions().match("*" + pattern + "*").count(1000).build();
		Cursor<String> cursor = this.setOperations.scan(redisKey, scanBuild);

		Set<String> set = new HashSet<>();
		while (cursor.hasNext()) {
			String text = cursor.next();
			if (onlyKey) {
				text = text.substring(text.lastIndexOf('[') + 1, text.lastIndexOf(']'));
			}
			set.add(text);
		}
		cursor.close();

		return set;
	}

	/**
	 * Keys of scan.
	 *
	 * @param redisKey Redis key.
	 * @param pattern  Scan pattern.
	 */
	public Set<String> scanKeys(final String redisKey, final String pattern) throws IOException {
		return this.scan(redisKey, pattern, true);
	}

	/**
	 * Delete the current db
	 *
	 */
	public void flushDb() {
		this.redisTemplate.getConnectionFactory().getConnection().flushDb();
	}
}