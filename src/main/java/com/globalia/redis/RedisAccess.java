package com.globalia.redis;

import com.globalia.Context;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class RedisAccess {

	@Getter
	private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	public static final String REDIS_KEY_FORMAT = "%s:%s";
	public static final String REDIS_KEYHASH_FORMAT = "%s¬%s";

	@Autowired
	private Context context;

	public String getRediskey(final String redisKey) {
		return String.format(RedisAccess.REDIS_KEY_FORMAT, this.context.getEnvironment(), redisKey);
	}
}
