package com.globalia.redis;

/**
 * Abstrac class defined to implement access to redis repository to find items.
 *
 * @param <T> Generic Type.
 */
public abstract class FindAllRedis<T> extends BaseRedis implements IFindAll<T> {
}