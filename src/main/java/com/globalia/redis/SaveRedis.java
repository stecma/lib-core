package com.globalia.redis;

/**
 * Abstrac class defined to implement access to redis repository to save item.
 *
 * @param <T> Generic Type.
 */
public abstract class SaveRedis<T> extends BaseRedis implements ISave<T> {
}