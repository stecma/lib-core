package com.globalia.redis;

import com.globalia.json.JsonHandler;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstrac class defined to implement the access to redis repository.
 */
public abstract class BaseRedis {

	@Getter
	@Autowired
	private RedisClient client;
	@Getter
	@Autowired
	private JsonHandler jsonHandler;
}