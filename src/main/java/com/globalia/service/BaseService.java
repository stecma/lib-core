package com.globalia.service;

import com.globalia.monitoring.MonitorHandler;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstrac class defined to implement the API CRUD services.
 */
public abstract class BaseService {

	@Getter
	@Autowired
	private MonitorHandler handler;
}