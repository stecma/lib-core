package com.globalia.service;

/**
 * Abstrac class defined to implement the API CRUD services to save data.
 *
 * @param <T> Generic Type.
 */
public abstract class SaveService<T> extends BaseService implements ISave<T> {
}