package com.globalia.service;

/**
 * Abstrac class defined to implement the API CRUD services to find data.
 *
 * @param <T> Generic Type.
 */
public abstract class FindAllService<T> extends BaseService implements IFindAll<T> {
}